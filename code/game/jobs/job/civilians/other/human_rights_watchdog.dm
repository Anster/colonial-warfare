/datum/job/civilian/hrwatchdog
	title = JOB_HUMAN_RIGHTS_WATCHDOG
	total_positions = 1
	spawn_positions = 1
	selection_class = "job_hr"
	flags_startup_parameters = ROLE_ADD_TO_DEFAULT|ROLE_ADD_TO_MODE
	supervisors = "USCM High Command / United Planets"
	gear_preset = "USCM Human Rights Watchdog (HRW)"

/datum/job/civilian/hrwatchdog/generate_entry_message(mob/living/carbon/human/H)
	entry_message_body = "As a trained lawyer, your job is to ensure that Military Police and the CO do not violate the rights of USCM personnel while applying Marine Law. You stand between the MPs and the accused during brig processing, giving prisoners a voice and maybe even a shoulder to cry on. When there are no incidents shipside, you are expected to document and report Human Rights violations committed by any parties during deployment. Remember, you are a civilian - your skills with the pen, legalese, and camera are what they pay you for."
	return ..()

AddTimelock(/datum/job/civilian/hrwatchdog, list(
	JOB_POLICE_ROLES = 5 HOURS
))


/obj/effect/landmark/start/warden
	name = JOB_HUMAN_RIGHTS_WATCHDOG
	job = /datum/job/civilian/hrwatchdog
